import json
from nose.tools import *

from tests import test_app

def check_content_type(headers):
  eq_(headers['Content-Type'], 'application/json')

def test_route():
  rv = test_app.get('/')
  check_content_type(rv.headers)
  resp = json.loads(rv.data)

  ## Make sure we get a response
  eq_(rv.status_code,200)

  ## make sure hello ==  world
  eq_(resp["hello"],"world")